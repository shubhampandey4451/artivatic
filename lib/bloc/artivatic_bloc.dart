
import 'dart:async';
import 'package:flutter/cupertino.dart';

import '../network/api_handler.dart';
import 'package:rxdart/rxdart.dart';



class ArtivaticBloc {
  late BuildContext context;
  late ApiHandler _apiHandler;

  StreamController<bool> progressLoaderController = BehaviorSubject<bool>();

  Stream<bool> get progressLoaderStream => progressLoaderController.stream;

  final StreamController _sApiResponse = StreamController();

  final StreamController _sApiError = StreamController();

  Stream get apiResponse => _sApiResponse.stream;

  Stream get apiError => _sApiError.stream;

  ArtivaticBloc(this.context) {
    _apiHandler = ApiHandler(context);
    _setApiObservable();
  }


  void getData() async {
    Map<String, dynamic> data = {
    };
    _apiHandler.getDataFromAPI(data);
  }

  onDispose() {
    _apiHandler.onDispose();
    _sApiResponse.close();
    _sApiError.close();
  }

  void _setApiObservable() {
    _apiHandler.onApiSuccess.listen((map) {
      _sApiResponse.sink.add(map);
    });

    _apiHandler.onApiError.listen((e) {
      _sApiError.sink.add(e);
    });
  }
}