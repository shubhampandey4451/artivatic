import 'dart:async';
import 'dart:io';

import 'package:artivatic/bloc/artivatic_bloc.dart';
import 'package:artivatic/modal/ArtivaticModal.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

import 'modal/Rows.dart';
import 'network/api_constant.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Artivatic Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: const MyHomePage(title: 'Artivatic Demo'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});
  final String title;
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}


class _MyHomePageState extends State<MyHomePage> {
  late ArtivaticBloc _artivaticBloc;
  var appTitle = '';
  final StreamController<List<Rows>> _artModal =
  BehaviorSubject();
  static Future<bool> checkConnectivity() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result.first.rawAddress.isNotEmpty) {
        return true;
      } else {
        return false;
      }
    } on SocketException catch (_) {
      return false;
    }
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _artivaticBloc = ArtivaticBloc(context);
    WidgetsBinding.instance.addPostFrameCallback((_) =>
        onPostFrameCallback(context));
  }
  onPostFrameCallback(BuildContext context) {
    checkConnectivity().then((value) {
      if (value) {
         searchList.clear();
        _artivaticBloc.getData();
      }
    });
    setObservables();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(appTitle),
      ),
      body: RefreshIndicator(
          onRefresh: () async {
          checkConnectivity().then((value) {
          if (value) {
             searchList.clear();
            _artivaticBloc.getData();
          }
        });
      },
      child: Column(
        children: [
          searchWidget(),
           str != ''
              ? Expanded(
                child: ListView(
                 shrinkWrap: true,
                 scrollDirection: Axis.vertical,
                 children: searchList.map((s){
                   if(s['title'].contains(str)) {
                     return Container(
                       padding: const EdgeInsets.all(10),
                       decoration: const BoxDecoration(
                         color: Colors.white,
                       ),
                       child: InkWell(
                         onTap: (){
                           // clearAndFilterAPI(cat);
                         },
                         child: Row(
                           crossAxisAlignment: CrossAxisAlignment.start,
                           children: [
                             SizedBox(
                               height: 150,
                               width: 150,
                               child: CachedNetworkImage(
                                 imageUrl: s['image']??"",
                                 fit: BoxFit.fill,
                                 progressIndicatorBuilder: (context, url, downloadProgress) =>
                                     SizedBox(height: 30, width: 30,
                                       child: CircularProgressIndicator(value: downloadProgress.progress),),
                                 errorWidget: (context, url, error) => const Icon(Icons.error),
                               ),
                             ),
                             const SizedBox(width: 10,),
                             Expanded(child: Column(
                               mainAxisSize: MainAxisSize.min,
                               crossAxisAlignment: CrossAxisAlignment.start,
                               children: [
                                 Flexible(child: Text(s['title']??"",
                                   style: const TextStyle(
                                       fontSize: 15,
                                       fontWeight: FontWeight.bold
                                   ),)),
                                 const SizedBox(height: 10,),
                                 Flexible(child: Text(s['desc']??"",
                                   style: const TextStyle(
                                       fontSize: 13,
                                       fontWeight: FontWeight.w500
                                   ),))
                               ],
                             ))
                           ],
                         ),
                       ),
                     );
                   }
                   else {
                     return const SizedBox(height: 0,);
                   }
                 }).toList()
             ),
                )
              : StreamBuilder<List<Rows>>(
                 stream: _artModal.stream,
                  builder: (context, snapshot) {
                   if (snapshot.hasData) {
                  var artivaticData = snapshot.data;
                  return Expanded(
                    //height: MediaQuery.of(context).size.height*.8,
                    child: ListView.separated(
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      itemCount: artivaticData?.length??0,
                      separatorBuilder: (_,__)=> const SizedBox(height: 10,),
                      itemBuilder: (context, index) {
                        var artData = artivaticData?[index];
                        return Container(
                          padding: const EdgeInsets.all(10),
                          decoration: const BoxDecoration(
                            color: Colors.white,
                          ),
                          child: InkWell(
                            onTap: (){
                              // clearAndFilterAPI(cat);
                            },
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: 150,
                                  width: 150,
                                  child: CachedNetworkImage(
                                    imageUrl: artData?.imageHref??"",
                                    fit: BoxFit.fill,
                                    placeholder: (context, url) => Container(
                                      alignment: Alignment.center,
                                      height: 30,width: 30,
                                      child: const CircularProgressIndicator(
                                          strokeWidth: 2,) // you can add pre loader iamge as well to show loading.
                                    ),
                                    errorWidget: (context, url, error) => const Icon(Icons.error),
                                  ),
                                ),
                                const SizedBox(width: 10,),
                                Expanded(child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Flexible(child: Text(artData?.title??"",
                                      style: const TextStyle(
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold
                                      ),)),
                                    const SizedBox(height: 10,),
                                    Flexible(child: Text(artData?.description??"",
                                      style: const TextStyle(
                                          fontSize: 13,
                                          fontWeight: FontWeight.w500
                                      ),))
                                  ],
                                ))
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                  );
                } else {
                    return Expanded(child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: const [
                          SizedBox(height: 30,width: 30,child: CircularProgressIndicator(strokeWidth: 2,),),
                          SizedBox(height: 10,),
                          Text("Please Wait! \nData is loading ...",
                            textAlign: TextAlign.center,
                            style: TextStyle(fontWeight: FontWeight.w400,
                                fontSize: 24),)
                        ],
                      ),
                    );
                }
              })
        ],
      )
     )
    );
  }

  List searchList=[];
  List<String> searchWords = [];
  var arr=[];
  String str = '';
  final FocusNode _searchNode = FocusNode();
  TextEditingController searchController = TextEditingController();
  String capitalize(String? s) => s![0].toUpperCase() + s.substring(1);

  Widget searchWidget(){
    return Container(
      margin: const EdgeInsets.all(10),
      padding: const EdgeInsets.all(10),
     decoration: BoxDecoration(
       borderRadius: BorderRadius.circular(10),
       color: Colors.grey.shade200
     ),
    child: TextFormField(
      style: const TextStyle(
        color: Colors.black,
        fontSize: 15,
      ),
      focusNode: _searchNode,
      controller: searchController,
      onChanged: (value){
        setState(() {
          searchWords = value.split(' ');
          if(value.isNotEmpty){
          str = searchWords.isNotEmpty
              ? capitalize(searchWords[searchWords.length - 1])
              : '';
        }
          else{
            str = '';
          }});

      },
      autofocus: false,
      showCursor: true,
      cursorColor: Colors.black,
      decoration: const InputDecoration(
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.black),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.black),
          ),
          hintText:"Search Here",
          hintStyle: TextStyle(
            color: Colors.black,
            fontSize: 15,
          ),
          fillColor: Colors.black54,
          border: InputBorder.none
      ),
    ),);
  }

  void setObservables() {
    _artivaticBloc.apiResponse.listen((map) {
      var apiType = map['request_code'];
      switch (apiType) {
        case ApiType.apiType:
          var data = ArtivaticModal.fromJson(map);
          _artModal.add(data.rows);
          for(int i = 0; i < data.rows.length; i++){
            setState(() {
              appTitle = data.title??"";
            });
            if(data.rows[i].title != null) {
              searchList.add({
                "title": data.rows[i].title,
                "image": data.rows[i].imageHref,
                "desc": data.rows[i].description,
              });
            }
          }
          break;
      }
    });

    //error listener
    _artivaticBloc.apiError.listen((error) {
    });
  }
}
