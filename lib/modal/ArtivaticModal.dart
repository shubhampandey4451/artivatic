import 'Rows.dart';

class ArtivaticModal {
  ArtivaticModal({
      required this.title,
      required this.rows,});

  ArtivaticModal.fromJson(dynamic json) {
    title = json['title'];
    if (json['rows'] != null) {
      rows = [];
      json['rows'].forEach((v) {
        rows!.add(Rows.fromJson(v));
      });
    }
  }
  String? title;
  late List<Rows> rows;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['title'] = title;
    if (rows != null) {
      map['rows'] = rows!.map((v) => v.toJson()).toList();
    }
    return map;
  }

}