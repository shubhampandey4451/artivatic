class Rows {
  Rows({
      required this.title,
      required this.description,
      required this.imageHref,});

  Rows.fromJson(dynamic json) {
    title = json['title'];
    description = json['description'];
    imageHref = json['imageHref'];
  }
  String? title;
  String? description;
  String? imageHref;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['title'] = title;
    map['description'] = description;
    map['imageHref'] = imageHref;
    return map;
  }

}