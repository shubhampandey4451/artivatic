///API REQUEST CODES
class ApiType {

  static const int apiType = 10001;
}

/// API Base Url
///
/// https://devapi.bakerindiaapp.com/api/appointment/addAppointment
class BaseUrl {


}

///API Url's End point
class ApiEndPoint {
  static const String apiUrl = 'https://run.mocky.io/v3/c4ab4c1c-9a55-4174-9ed2-cbbe0738eedf';
}
