import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter/widgets.dart';
import 'api_constant.dart';
import 'api_provider.dart';
import 'api_response.dart';

class ApiHandler implements ApiResponse {
  BuildContext _context;

  final StreamController _onApiSuccess = StreamController();
  final StreamController _onApiError = StreamController();
  final StreamController _onApiLoader = StreamController();

  // API Error Stream
  Stream get onApiError => _onApiError.stream;

  Stream get onApiLoader => _onApiLoader.stream;

  // API Success Stream
  Stream get onApiSuccess => _onApiSuccess.stream;

  ApiHandler(this._context);

  /// Closes all the streams
  onDispose() {
    _onApiError.close();
    _onApiSuccess.close();
    _onApiLoader.close();
  }

  void getDataFromAPI(Map map) {
    ApiProvider provider = ApiProvider(this);
    provider.dioGet(_context, ApiEndPoint.apiUrl, ApiType.apiType, map);
  }

  /// On API error
  @override
  void onError(e) {
    _onApiError.sink.add(e);
  }

  /// On API Loader
  @override
  void onLoading(e) {
    _onApiLoader.sink.add(e);
  }

  // Root API
  /// On API success
  @override
  void onResponse(Response response, int apiType) {
    Map<String, dynamic> map = response.data;
    if (map.containsKey('rows')) {
      Map<String, dynamic> data = {};
      _onApiLoader.sink.add(true);
        data = map;
        print("sdfgh $data");
        data.putIfAbsent('request_code', () => apiType);
        if (!_onApiSuccess.isClosed) {
          _onApiSuccess.sink.add(data);
        }
    } else {
      try {
        Map<String, dynamic> error = {};
        _onApiLoader.sink.add(true);
        _onApiError.sink.add(error);

      } catch (error, stacktrace) {
        if (!_onApiError.isClosed) {
          Map<String, dynamic> error = {};
          _onApiError.sink.add(error);
        }
      }
    }
  }
}
