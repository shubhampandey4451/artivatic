import 'dart:convert';

import 'package:dio/dio.dart';


import 'api_response.dart';

class ApiProvider {
  late Dio _dio;

  late ApiResponse _apiCallback;

  ApiProvider(ApiResponse apiCallback) {
    _apiCallback = apiCallback;
    Map<String, dynamic> map = {};
    map.putIfAbsent('accept', () => 'application/json');
    BaseOptions options = BaseOptions(
        receiveTimeout: const Duration(milliseconds: 50000),
        sendTimeout: const Duration(milliseconds: 50000),
        connectTimeout: const Duration(milliseconds: 50000));
    options.contentType = Headers.formUrlEncodedContentType;
    options.headers = map;
    options.baseUrl = '';
    _dio = Dio();
    _dio.options = options;
  }


  String accessTokenData = '';


  /// Call this method to make GET API requests
  dioGet(context, String url, int apiType, request) async {
    try {
      Response response = await apiGetRequest(url, request);
      _apiCallback.onResponse(response, apiType);
    } catch (error) {
      return error;
    }
  }
  Future apiGetRequest(String url, request) async {
    final response = await _dio.get(url, queryParameters: request);
    return response;
  }

}
